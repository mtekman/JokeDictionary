import json
import string
import sqlite3
import urllib.request
import pandas
import umap.umap_ as umap
#import umap
import pickle
import matplotlib.pyplot as plt

from time import sleep
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.cluster import DBSCAN

class PullData:

    @staticmethod
    def cleanData(string):
        return(' '.join(
            (''.join(s for s in string.replace('\n',' ') if ord(s)>31 and ord(s)<126)).split()
        ))

    @staticmethod
    def getAuthorComment(post_id, author):
        url = "https://api.pushshift.io/reddit/comment/search/?link_id=%s&nest_level=1&author=%s" % (post_id, author)
        data_json = urllib.request.urlopen(url)
        data = data_json.read()
        encoding = data_json.info().get_content_charset('utf-8')

        # We assume the first top level comment by the author is that punchline
        # where all other comments by the user must just be replies (not top-level)
        body_tmp = json.loads(data.decode(encoding))['data']
        if len(body_tmp) == 0:
            return(None)
        return(body_tmp[0]['body'])

    @staticmethod
    def convertToNice(tmp):
        """Convert text to lowercase without punctuation"""
        wanted_chars = string.ascii_letters + string.digits + ' '
        return("".join([char.lower() for char in tmp if char in wanted_chars]))

    @staticmethod
    def getThreadData(tmp):
        # Check URL
        # - if it's not a reddit self url, then we forget it

        if tmp['url'].find("www.reddit.com") == -1:
            return None

        data = {
            "id" : tmp['id'],
            "author" : tmp['author'],
            "created" : tmp['created_utc'],
            "comments" : tmp['num_comments'],
            "url" : tmp['url'],
            "nsfw" : not(tmp['over_18']),
            "score" : tmp['score'],
            "title" : PullData.cleanData(tmp['title']),
            "ups" : -1,
            "downs" : -1,
            "body" : ""
        }
        if "ups" in tmp:data["ups"] = tmp["ups"]
        if "downs" in tmp:data["downs"] = tmp["downs"]

        # Convert joke to lowercase for clustering
        data["nice_title"] = PullData.convertToNice(data["title"])

        body=None
        if "selftext" in tmp:
            body = tmp["selftext"]
        else:
            sleep(3)
            # If body not available then just
            # scan top_5 for author comment
            body = PullData.getAuthorComment(data['id'], data['author'])

        if body is None or len(body)<2 or body=="[removed]":
            data["body"] = "None"
        else:
            data["body"] = PullData.cleanData(body)

        # Convert joke to lowercase for clustering
        data["nice_body"] = PullData.convertToNice(data["body"])

        return(data)

    @staticmethod
    def getDelay(delayextra=0):
        """Get sleeper between requests based on rate limit info"""
        data_json = urllib.request.urlopen("https://api.pushshift.io/meta")
        data = data_json.read()
        encoding = data_json.info().get_content_charset('utf-8')
        rate_limit = int(json.loads(data.decode(encoding))['server_ratelimit_per_minute'])
        delay = 60 / rate_limit
        delay += delayextra
        ##print(delay)
        return(delay)


    @staticmethod
    def runEpoch(size, epoch):
        url = "https://api.pushshift.io/reddit/search/submission/?subreddit=jokes&size=%d&after=%d" % (size, epoch)
        ##print(url)
        try:
            data_json = urllib.request.urlopen(url)
        except:
            print("Failed on:", url)
            print("Trying again in 60 seconds")
            sleep(60)
            return(PullData.runEpoch(size, epoch))

        data = data_json.read()
        encoding = data_json.info().get_content_charset('utf-8')
        json_data = json.loads(data.decode(encoding))['data']

        json_filt = []
        json_created = []
        for x in json_data:
            tmp = PullData.getThreadData(x)
            if tmp is None:
                continue
            json_filt.append(tmp)
            json_created.append(tmp['created'])

        return(json_filt, max(json_created) if len(json_created) > 0 else None)




class RedditStore:

    def initialiseLogFile(self):
        self.logname = "log_epoch.txt"
        logfile = open(self.logname, 'a')
        logfile.write("\n====")
        logfile.flush()
        logfile.close()

    def logThis(self, val, thrds):
        logfile = open(self.logname, 'a')
        logfile.write("\n%s -- %s" % (val, thrds))
        logfile.flush()
        logfile.close()


    def getLastEpoch(self):
        with open(self.logname, 'r') as logfile:
            ll = logfile.readlines()
            index = -1
            last = ll[index]
            while index > -100 and (last.startswith("====") or len(last)<2):
                # Crawl back
                index -= 1
                last = ll[index]

            return(int(last.split(' -- ')[0]))


    def getLastEpoch(self):
        logfile = open(self.logname, 'r')
        ll = logfile.readlines()

        index = -1
        last = ll[index]
        print(last, "RESIDENT")
        while index > -100 and (last.startswith("====") or len(last)<2):
            # Crawl back
            index -= 1
            last = ll[index]

        return int(last.split(' -- ')[0])


    def __init__(self, database="joke2.db", table="jokes",
                 poolsize = 20, threadpool=200, delayextra=0):
        '''
        Start pulling joke posts from reddit and store them in an sqlite db

        Parameters:
            database (str): location to store database
            table (str): name of database table to store table
            poolsize (int): how many pull requests to manage simultaneously
            threadpool (int): max number of concurrent pulls
            delayextra (int): Seconds of delay to add between requests. Set to 5 if
                              you encounter too many request errors.

            The threadpool is the number of threads to try to pull simultaneously
            This should ideally be much larger than the poolsize because many threads
            (external) will be skipped.
        '''
        self.table = table
        self.database = database
        self.conn = None

        self.createThreadTable()

        ## Reddit Data Puller
        pd = PullData()

        #curr_epoch = 1202650974  # first post
        #last_epoch = 1562965018 # latest post

        self.initialiseLogFile()
        curr_epoch = self.getLastEpoch()

        delay = pd.getDelay(delayextra)

        while True:
            threads, new_epoch = pd.runEpoch(threadpool, curr_epoch)

            if new_epoch is None:
                print("Last epoch reached")
                break

            n_thrds = self.insertMultiData(threads)
            sleep(delay)
            curr_epoch = new_epoch
            # Log only if successful
            print(curr_epoch)
            self.logThis(new_epoch, n_thrds)

        print(self.getNumThreads(), "threads")


    def createThreadTable(self):
        sql_create_threads_table = """CREATE TABLE IF NOT EXISTS %s (
            id pvarchar(16) primary key not null, title text not null, body text not null, author text not null, url text not null,
            created numeric not null, score integer not null, ups integer not null, downs integer not null, comments integer not null,
            nsfw boolean not null, nice_title text not null, nice_body text not null, UNIQUE(id));""" % self.table

        # create a database connection
        conn = sqlite3.connect(self.database)
        c = conn.cursor()
        c.execute(sql_create_threads_table)
        conn.commit()
        conn.close()


    def insertMultiData(self, multidata):
        sql = ''' INSERT OR IGNORE INTO %s(id,title,body,author,url,created,score,ups,downs,comments,nsfw,nice_title,nice_body)
                  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ''' % self.table

        def prepareTask(data):
            """This task is called by runNewPool"""
            task = (data["id"], data["title"], data["body"], data["author"], data["url"], data["created"],
                    data["score"], data["ups"], data["downs"], data["comments"], data["nsfw"], data["nice_title"], data["nice_body"])
            return(task)

        mdata = list(map(lambda x: prepareTask(x), multidata))

        conn = sqlite3.connect(self.database, timeout=20)
        c = conn.cursor()
        c.executemany(sql, mdata)
        conn.commit()
        conn.close()

        return(len(mdata))


    def getNumThreads(self):
        """Debugging. Test to see how much is inserted."""
        conn = sqlite3.connect(self.database)
        cur = conn.cursor()
        cur.execute("SELECT count(*) FROM %s" % self.table)
        rows = cur.fetchall()[0][0]
        conn.close()
        return(rows)


class Inspect:

    @staticmethod
    def getQuery(query, columns):
        conn = sqlite3.connect("joke2.db")
        cur = conn.cursor()
        cur.execute(query)
        rows = pandas.DataFrame(cur.fetchall(),
                                columns=columns)
        conn.close()
        return(rows)

    @staticmethod
    def getReposts():
        return(
            Inspect.getQuery("SELECT title, body,\
        COUNT(*) FROM jokes\
        GROUP BY title, body\
        HAVING COUNT(*) > 1",
        columns=["Title","Body","Reposted Amount"]))

    @staticmethod
    def flattenDatasets():
        table = Inspect.getQuery(
                "SELECT \
                id, nice_title || ' --- ' || nice_body \
                FROM jokes",
        columns=["id", "Data"]
            ).set_index("id")

        return(
            table,
            [x[0] for x in table.values.tolist()]
        )


class Analysis:

    @staticmethod
    def subsample(table, n_obs=1000, last=True):
        if last:
            tab = table.iloc[-n_obs:]
        else:
            tab = table.sample(n=n_obs)
        return(tab, [x[0] for x in tab.values.tolist()])
    
    @staticmethod
    def makeFeaturesIDF_lowMem(data, max_features=10000,
                        df_range=(2, 0.5), ngram_range=(2,3), names=False):
        from sklearn.pipeline import Pipeline
        pipe = Pipeline([("hash", HashingVectorizer(
            n_features=n_features,
            ngram_range=ngram_range,
            min_df=df_range[0],
            max_df=df_range[1],
            stop_words='english')),
                         ("tfid", TfidTransformer())]).fit(data)
        out = pipe.transform(data)
        return(out)


    @staticmethod
    def makeFeaturesIDF(data, max_features=10000,
                        df_range=(2, 0.5), ngram_range=(2,3), names=False):
        "This takes an incredible amount of memory when max_features is higher than > 100k."
        vectorizer = TfidfVectorizer(
            max_features=max_features,
            ngram_range=ngram_range,
            min_df=df_range[0],
            max_df=df_range[1],
            lowercase=False,  # already done and causes problems if true
            # 10M consumed all 120GB and stopped, trying 100K (segfaulted), trying 10K
            stop_words='english',
            use_idf=True
        )
        out = vectorizer.fit_transform(data)
        if names:
            return(out, vectorizer.get_feature_names())
        return(out)

    @staticmethod
    def dimensionReduction(X, pca_components=50):
        print("X.shape", X.shape)
        tsvd = TruncatedSVD(n_components=pca_components)
        X_redux = tsvd.fit_transform(X)
        print("X_redux.shape", X_redux.shape)
        return(X_redux)

    @staticmethod
    def perform2DEmbed(X_redux, neighs=15, dist=0.1):
        if len(X_redux) > 1e6:
            print("This can take > 24 hours.")
        reducer = umap.UMAP(
            n_neighbors=neighs,
            min_dist=dist
        )
        embedding = reducer.fit_transform(X_redux)
        print("embedding.shape", embedding.shape)
        return(embedding)

    @staticmethod
    def plotEmbed(embed, title, figsize=(18,16),
                  dpi=80, point=0.05, xlim=None, ylim=None, fname=None):
        fig, ax = plt.subplots(figsize=figsize, dpi=dpi, facecolor='w', edgecolor='k')
        ax.scatter(embed[:, 0], embed[:, 1], s=point)
        plt.gca().set_aspect('equal', 'datalim')
        plt.title(title, fontsize=24)

        if xlim:plt.xlim(xlim)
        if ylim:plt.ylim(ylim)

        if fname is not None:
            plt.savefig(fname)
        
        return(fig,ax)

    @staticmethod
    def annotatePlot(ax, embedding, table, xlims, ylims, every=1000):
        center_titles=[]
        for i, txt in enumerate(table.index):
            xx = embedding[:, 0][i]
            yy = embedding[:, 1][i]

            if xlims[0] <= xx <= xlims[1]:
                if ylims[0] <= yy <= ylims[1]:
                    center_titles.append(txt)
                    if i%every==0:
                        ax.annotate(txt, (xx, yy))

        print("Datapoints:", len(center_titles))
        return(table[table.index.isin(center_titles)])

    @staticmethod
    def cluster(X, eps=0.3, min_samples=10):
        # https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html#sphx-glr-auto-examples-cluster-plot-dbscan-py
        db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
        return(db)


##' The below code was taken from https://gist.github.com/kmike/7814472 as a workaround to the very memory intensive TFIDVectorizer
##' More info can be found at this blog here: https://blog.scrapinghub.com/2014/03/26/optimizing-memory-usage-of-scikit-learn-models-using-succinct-tries
## hack to store vocabulary in MARISA Trie
class _MarisaVocabularyMixin(object):

    def fit_transform(self, raw_documents, y=None):
        super(_MarisaVocabularyMixin, self).fit_transform(raw_documents)
        self._freeze_vocabulary()
        return super(_MarisaVocabularyMixin, self).fit_transform(raw_documents, y)

    def _freeze_vocabulary(self):
        if not self.fixed_vocabulary_:
            self.vocabulary_ = marisa_trie.Trie(self.vocabulary_.keys())
            self.fixed_vocabulary_ = True
            del self.stop_words_

class MarisaCountVectorizer(_MarisaVocabularyMixin, CountVectorizer):
    pass

class MarisaTfidfVectorizer(_MarisaVocabularyMixin, TfidfVectorizer):
    def fit(self, raw_documents, y=None):
        super(MarisaTfidfVectorizer, self).fit(raw_documents)
        self._freeze_vocabulary()
        return super(MarisaTfidfVectorizer, self).fit(raw_documents, y)
