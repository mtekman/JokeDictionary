#+TITLE: Quickstart

* Install and Enable the Environment

  Create the =virtualenv=

  #+begin_src bash :session install
    virtualenv env && source env/bin/activate
  #+end_src

  Once enabled, install the following modules

  #+begin_src bash :session install
    pip3 install pandas umap-learn matplotlib sklearn jupyterlab
  #+end_src

  And now tell Emacs where to look:

  #+begin_src elisp
    (setq python-shell-virtualenv-path
          "/set/your/path/to/JokeDictionary/env/")
  #+end_src

* Run The Analysis

** Populate the Database with Reddit Posts

   Here we populate a SQLite3 database with posts from =r/jokes=, starting from the dawn of time to the current (<2020-12-15 Tue>).

   This is performed using the [[https://api.pushshift.io/][Pushshift API]]. This can take hours depending on what =delayextra= parameter you set. If set to 0, then this delay is taken from the recommended interval time retrieved from https://api.pushshift.io/meta. However, this does not appear to be working well in recent times, so I set =delayextra=5= to add a 5 second interval to each request. 

   #+begin_src python :session run
     import jokedictfuncs as jdf
     rs = jdf.RedditStore(delayextra=2)
   #+end_src

   If this fails, just run it again and it should start off where it left off by reading the last entry in the =log_epoch.txt= file.
   
** Process Data

   Now we take the data from our database (~700MB) and flatten it so that all we have is the joke ID (reddit post ID), and a combination of the joke title and joke body in a single column (separated by " --- ")
   
   #+begin_src python :session run
    table, data = Inspect.flattenDatasets()
   #+end_src

   Let us now verify that some jokes do indeed exist.

   #+begin_src python :session run
     table.loc["srv81"]
     table.loc["srvv0"]
     table.loc["srww5"]
     len(table)
   #+end_src
   

** Feature Extraction, Dimension Reduction, Embedding

   So we have ~1.27 million jokes sharing a lot of words between them that aren't that useful to distinguishing differences between jokes (e.g extremely common words like "the" and "a" should be ignored). One way to get around this is to use a vectorizer which skips stopwords, and deals with ngrams (e.g. bigrams like "the dog" and "dog ate").

   TFidvectorizer would do this, but it takes up a shitton of memory if we use all our data. TFidvectorizer is the same as a CountVectorizer followed by a TfidTransformer, but CountVectorizer can be swapped in with a HashingVectorizer for memory saving purposes (TODO: Implement this).

   For now, let's just subsample our dataset to just 10,000.

   #+begin_src python :session run
     subtable, corpus = Analysis.subsample(table, n_obs=10000, last=False)
   #+end_src

   #+RESULTS:

   

   #+NAME:TestClust
   #+begin_src python :session *Python* :var n1=1 :var n2=1 :var fname="test.png" :results value file
     X, dimnames = Analysis.makeFeaturesIDF(corpus, max_features=len(corpus), df_range=(1, 0.5),
                                            ngram_range=(n1,n2), names=True)
     X_redux = Analysis.dimensionReduction(X, pca_components=50)
     embedding = Analysis.perform2DEmbed(X_redux)
     Analysis.plotEmbed(embedding, "Test", fname=fname)
     fname
   #+end_src

   #+CALL:TestClust(n1=2,n2=3,fname="2_3.png")

   #+RESULTS:
   [[file:2_3.png]]

   #+CALL:TestClust(n1=1,n2=1,fname="1_1.png")

   #+RESULTS:
   [[file:1_1.png]]

   It looks like ngrams of 1,1 actually work the best for getting subclusters, but we will need to explore this more later, and also confirm that some of the smaller clusters genuinely cluster well.


* Zoomed Region

  For the 1,1 plot -- let's take a look at some of the jokes in a zoomed region
  
  #+begin_src python :session *Python* 
    fig, ax = Analysis.plotEmbed(embedding, "Test", ylim=(8,11), xlim=(-2,2), fname="zoomed.png")
    jokes_in_region = Analysis.annotatePlot(ax, embedding, subtable, (-2,2), (8,11))
  #+end_src
  
  Looks like trump and putin jokes

  Let's look at another sub region of this

  #+begin_src python :session *Python* :results value verbatim
    jokes_in_region = Analysis.annotatePlot(ax, embedding, subtable, (-2,-1.5), (8.75,9))
    jokes_in_region.Data.values
  #+end_src

  #+RESULTS:
  #+begin_example
  ['what does a neovagina have in common with a pussy --- none'
   'what does a reposted joke and your mamas tits have in common --- seen it seen it dont care for either'
   'what does the uss enterprise have in common with tp --- they both circle uranus wiping out clingons'
   'what does a pair of dirty socks have in common with a land war in asia --- the stench of de feet'
   'what does my dck and my wife have in common --- they both get beat'
   'q what does the bermuda triangle and blondes have in common --- q what does the bermuda triangle and blondes have in common a theyve both swallowed a lot of semen'
   'what does your computer and your mom have in common --- the both are great at downloading'
   'what does the maffia and --- eating pussy have in common one slip of the tongue and youre in deep shit'
   'what does terry fox and adolf hitler have in common --- neither could finish a race'
   'what does the moon and clemson have in common --- none'
   'what does an escalade and artificial intelligence have in common --- max headroom'
   'what does a hot super model have in common with this joke --- you dont get it'
   'what does italy and frankenstein have in common --- they both have a bad history with the mob'
   'what does kmart and michael jackson have in common --- both have little boys shorts at half off'
   'what does a rubiks cube and a penis have in common --- the longer you play with them the harder they get'
   'what does jeb bush have in common with a threesome --- a lot of people think three bushes is one bush too many'
   'what does a dildo and tofu have in common --- theyre both meat substitutes'
   'what does the enterprise from star trek and toilet paper have in common --- they both fly arround uranus picking off clingons'
   'whats does a deppressed mom and a dad have in common --- none'
   'q what does cranial nerve 7 and the bartholins glands have in common --- a they both provide taste to the anterior 23 of the tongue'
   'what does a mcu marvel fan and a friendzoned guy have in common --- they are both always excited'
   'what does disney have in common with a guy in an outhouse in chicago --- theyre both making a frozen number two'
   'what does australia have in common with rthedonald --- none'
   'what does britain and spain have in common --- they both colonized the world'
   'what does a tornado and a redneck divorce have in common --- in the end someones always going to lose a trailer'
   'what does yeast and a person from alabama have in common --- both are inbred']
  #+end_example

  No relation in theme really, but they are all very formulaic so perhaps that's relation enough



  


  