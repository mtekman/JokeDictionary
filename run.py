#!/usr/bin/env python

import jokedictfuncs as jdict
import umap
import pickle
import datetime

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD

print("Flattening datasets", flush=True)
print(datetime.datetime.utcnow())
table, data = jdict.Inspect.flattenDatasets()
print(datetime.datetime.utcnow())

print("Performing Dimension Reduction", flush=True)
vectorizer = TfidfVectorizer(
    ngram_range=(2,3),
    lowercase=False,  # already done and causes problems if true
    min_df=2,max_df=0.5,
    # 10M consumed all 120GB and stopped, trying 100K (segfaulted), trying 10K
    max_features=10000,
    stop_words='english',
    use_idf=True)

print("Vectorising Dataset", flush=True)
print(datetime.datetime.utcnow())
X = vectorizer.fit_transform(data)
print(datetime.datetime.utcnow())


print("Doing DimRed", flush=True)
print(datetime.datetime.utcnow())
tsvd = TruncatedSVD(n_components=50)
X_redux = tsvd.fit_transform(X)
print(datetime.datetime.utcnow())
print("DimRed:", X_redux.shape, flush=True)


print("Performing UMAP fit", flush=True)
print(datetime.datetime.utcnow())
reducer = umap.UMAP()
embedding = reducer.fit_transform(X_redux)
print(datetime.datetime.utcnow())
print("UMAP:", embedding.shape, flush=True)

print("Saving pickle", flush=True)
print(datetime.datetime.utcnow())
with open("embedding.pickle", "wb") as efile:
    pickle.dump([X,embedding], efile)
print(datetime.datetime.utcnow())

